package tests;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utils.DBConnect;

public class TestCase_003 extends BaseTest{

	@Test
	public void DBTest() throws ClassNotFoundException, SQLException {
		
		
		driver.quit();
		ExtentTest childTest = pNode.createNode("DataBase Test");	
		String sql = "select * from employees";
		
		childTest.info("SQL Query Used : " + sql);
		
		DBConnect database = new DBConnect();
		ResultSet data = database.getDataBaseRecord(sql);
		
		int colCount = data.getMetaData().getColumnCount();
		
		
		for(int i=1;i<= colCount;i++) {
			
			childTest.info(" Query Result : " + data.getMetaData().getColumnName(i));
			
		}
		
		
		while(data.next()) {
			childTest.info(" Query Result : " + data.getString(1) + " :::: "+ data.getString(2)+" :::: "+ data.getString(3));
		}
		
		
	}
	
}
