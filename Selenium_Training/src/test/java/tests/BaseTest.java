package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import com.aventstack.extentreports.ExtentTest;

import utils.DriverFactory;
import utils.ReadConfig;
import utils.Reporter;


public class BaseTest {

	
	protected static ExtentTest pNode;
	protected  WebDriver driver;
	ReadConfig readConfig = new ReadConfig();
	
	public String baseURL = readConfig.getApplicationURL();
	public String browserName=readConfig.getBrowser();

	@BeforeSuite
	public void initReport() {
		pNode = Reporter.createReport().createTest("TEST RUN REPORT");	
	}

	@BeforeClass
	protected void setUp() {

			driver=DriverFactory.getDriver(browserName);	
	}

	@AfterClass
	protected void tearDown() {
		// Quit the driver
		driver.quit();
	}

	@AfterSuite
	public void closeReport() {
		Reporter.quitExtent();
	}

}