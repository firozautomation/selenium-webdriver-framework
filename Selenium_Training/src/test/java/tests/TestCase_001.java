package tests;


import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utils.ExcelReadWrite;
import pageObjects.MercuryToursHomePage;
import pageObjects.MercuryToursRegistrationPage;

public class TestCase_001 extends BaseTest{


	@Test
	public void contactTest1_a() {
		
		driver.get(baseURL);
		ExtentTest childTest = pNode.createNode("Contact Test");	  
		MercuryToursHomePage home = new MercuryToursHomePage(driver,childTest);
		home.clickContact();

	}

	@Test
	public void contactTest1_b() {

		driver.get(baseURL);
		ExtentTest childTest = pNode.createNode("Contact Test");	  
		MercuryToursHomePage home = new MercuryToursHomePage(driver,childTest);
		home.clickContact();

	}


	@Test
	public void registerTest1_c(){

		driver.get(baseURL);
		ExtentTest childTest = pNode.createNode("User Registration Test");
		MercuryToursHomePage home = new MercuryToursHomePage(driver,childTest);
		home.clickRegister();

		ExcelReadWrite dataSheet = new ExcelReadWrite(System.getProperty("user.dir")+"\\src\\test\\resources\\Data.xlsx","TestCase_001");

		String firstName = dataSheet.getCellData("TestCase_001", "FirstName",2);
		String lastName = dataSheet.getCellData("TestCase_001", "LastName",2);
		String phoneNumber = dataSheet.getCellData("TestCase_001", "PhoneNumber",2);
		String emailId = dataSheet.getCellData("TestCase_001", "EmailID",2);
		String address = dataSheet.getCellData("TestCase_001", "Address",2);
		String city = dataSheet.getCellData("TestCase_001", "City",2);
		String password = dataSheet.getCellData("TestCase_001", "Password",2);

		MercuryToursRegistrationPage regPage = new MercuryToursRegistrationPage(driver,childTest);
		regPage.setCustomerDetails(firstName, lastName, phoneNumber, emailId, address, city, password);
		regPage.clickRegister();


	}


}