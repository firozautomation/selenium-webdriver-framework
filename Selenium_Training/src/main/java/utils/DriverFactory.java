package utils;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class DriverFactory {

	private static final String FIREFOX = "firefox";
	private static final String CHROME = "chrome";


	public static WebDriver getDriver(String browser) {

		WebDriver driver= null;

		if (browser.equalsIgnoreCase(CHROME)) {

			// initialize driver
			String exePath = System.getProperty("user.dir")+"\\drivers\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver",exePath);
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			//driver.manage().window().maximize();

		}

		else if (browser.toLowerCase().equals(FIREFOX)) {

			String exePath = System.getProperty("user.dir")+"\\drivers\\geckodriver.exe";
			System.setProperty("webdriver.gecko.driver",exePath);
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			//driver.manage().window().maximize();

		} else {
			/*
            Create Driver for Default Browser
			 */
		}

		return driver;
	}
}
