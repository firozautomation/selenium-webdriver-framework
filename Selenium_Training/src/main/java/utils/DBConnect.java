package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnect {

	public Connection conn;
	public Statement stmt;

	public Statement getStatement() throws ClassNotFoundException,SQLException{

		String Driver="oracle.jdbc.driver.OracleDriver";
		String connectionString="jdbc:oracle:thin:@localhost:1521:xe";
		String userid="hr";
		String passwd="hr";

		Class.forName(Driver);
		conn = DriverManager.getConnection(connectionString, userid, passwd);
		stmt= conn.createStatement();
		return stmt;


	}

	public ResultSet getDataBaseRecord(String query) throws ClassNotFoundException, SQLException {
		ResultSet data = getStatement().executeQuery(query);
		return data;
	}



}
