package utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {

	public Properties prop;
	
	public ReadConfig(){
		
	File configFile = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\test-config.properties"); 
		try {
			FileInputStream fis = new FileInputStream(configFile);
			prop = new Properties();
			prop.load(fis);
		}
		
		catch(Exception e) {
			System.out.println("Exception is : " + e.getMessage());
		}
	}
	
	
	public String getApplicationURL() {
		String url = prop.getProperty("app.url");
		return url;
	}
	
	public String getBrowser() {
		String browser = prop.getProperty("browser.name");
		return browser;
	}
	
  
}
