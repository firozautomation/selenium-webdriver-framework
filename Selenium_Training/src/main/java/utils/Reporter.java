package utils;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Reporter {
	
	public static ExtentReports extent;
	//public static final String baseURL = ConfigReader.getInstance().getProperty("app.url");
	//public static final String browserName =  ConfigReader.getInstance().getProperty("browser.name");
	
	public static ExtentReports createReport() {

		
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(new Date());
		String fileName ="test-result" +"-"+ timeStamp;
		String reportPath = "D:\\Eclipse_Projects\\Selenium_Training\\" + fileName  + ".html";

		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportPath);
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setDocumentTitle(fileName);
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setReportName(fileName);

		extent = new ExtentReports();
		extent.setAnalysisStrategy(AnalysisStrategy.TEST);
		extent.attachReporter(htmlReporter);
		//extent.setSystemInfo("Application URL", baseURL);
		//extent.setSystemInfo("Browser", browserName);

		return extent;
	}

	public static void quitExtent() {
		extent.flush();
		extent = null;
	}

}
