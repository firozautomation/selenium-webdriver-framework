package pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.aventstack.extentreports.ExtentTest;

public class MercuryToursHomePage extends BasePage{
	
	
	// Locators
	@FindBy(linkText="REGISTER")
    WebElement lnkRegister;
	
	@FindBy(linkText="CONTACT")
    WebElement lnkContact;
	
	@FindBy(name="userName")
    WebElement txtUserName;
	
	@FindBy(name="password")
    WebElement txtPassword;
	
	@FindBy(name="login")
    WebElement btnLogin;
	
	//Methods
	public void clickRegister() {
		clickOnElement(lnkRegister,"Register Link");
	}
	
	public void clickContact() {
		clickOnElement(lnkContact,"Contact Link");
	}
	
	public void loginDetails() {
		setText(txtUserName,"tester","User Name");
		setText(txtPassword,"passwd","Password");
		clickOnElement(btnLogin,"Login Button");
		
	}
	
	//Page Constructor	
	public MercuryToursHomePage(WebDriver driver, ExtentTest t1) {
		super(driver,t1);
	}
	
}