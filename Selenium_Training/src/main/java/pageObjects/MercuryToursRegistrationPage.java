package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.aventstack.extentreports.ExtentTest;

public class MercuryToursRegistrationPage extends BasePage{
	
	 // WebDriver driver;

		// Locators
		@FindBy(name="firstName")
	    WebElement txtFirstName;
		
		@FindBy(name="lastName")
	    WebElement txtLastName;
		
		@FindBy(name="phone")
	    WebElement txtPhoneNumber;
		
		@FindBy(id="userName")
	    WebElement txtEmailID;
		
		@FindBy(name="address1")
	    WebElement txtAddressLine1;
		
		@FindBy(name="city")
	    WebElement txtCity;
		
		@FindBy(id="email")
	    WebElement txtUserID;
		
		@FindBy(name="password")
	    WebElement txtPassword;
		
		@FindBy(name="confirmPassword")
	    WebElement txtConfirmPassword;
		
		@FindBy(name="register")
	    WebElement btnRegister;
		

		//Methods
		public void setCustomerDetails(String firstName,String lastName,String phoneNumber, String emailId,
										String address, String city,String password){
			
			setText(txtFirstName,firstName,"First Name");
			setText(txtLastName,lastName,"Last Name");
			setText(txtPhoneNumber,phoneNumber,"Phone Number");
			setText(txtEmailID,emailId,"Email ID");
			setText(txtAddressLine1,address,"Address");
			setText(txtCity,city,"City");
			setText(txtUserID,emailId,"First Name");
			setText(txtPassword,password,"First Name");
			setText(txtConfirmPassword,password,"First Name");
								
		}
		
		public void clickRegister() {
			clickOnElement(btnRegister,"Register Button");
		}
		
	//Page Constructor	
		public MercuryToursRegistrationPage(WebDriver driver,ExtentTest t1) {
			super(driver, t1);			
		}
			

}
